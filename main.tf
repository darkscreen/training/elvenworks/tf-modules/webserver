//Create Basttion EC2 with IAM Role for SSM
resource "aws_security_group" "ec2-sg" {
  name        = var.ec2_sg_name
  description = "Allow all traffic from VPC CIDR and Open World SSH/HTTP to EC2"
  vpc_id      = var.vpc_id

  ingress {
    description      = "Allow all from VPC CIDR"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = [var.vpc_cidr]
  }
  ingress {
    description      = "Allow OpenWorld SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    description      = "Allow OpenWorld HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.ec2_sg_name
    terraformed = "true"
  }
}

resource "aws_instance" "webserver" {
  ami = var.ami
  instance_type = var.instance_type
  availability_zone = var.az
  subnet_id = var.subnet_id
  associate_public_ip_address = true
  key_name = var.key_pair_name
  vpc_security_group_ids = [ aws_security_group.ec2-sg.id ]
  root_block_device {
    volume_size = "100"
  }
  tags = {
    Name = var.ec2_name
    terraformed = "true"
    Owner = "lazevedo"
    Webserver = "true"
  }
}