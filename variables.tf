//SG Variables
variable "ec2_sg_name" {
  default = "webserver_sg"
}
variable "vpc_id" {}
variable "vpc_cidr" {}
variable "az" {}

//EC2 Variables
variable "ami" {
    default = "ami-00874d747dde814fa"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "subnet_id" {}
variable "ec2_name" {
  default = "WebServer"
}
variable "key_pair_name" {}